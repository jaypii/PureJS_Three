'use strict';

/* global THREE */

function main() {
   const canvas = document.querySelector('#c');
   const renderer = new THREE.WebGLRenderer({canvas});
   renderer.autoClearColor = false;

   // Set perspective camera
   const fov = 75;
   const aspect = 2;  // the canvas default
   const near = 0.1;
   const far = 100;
   const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
   camera.position.z = 20;

   const controls = new THREE.OrbitControls(camera, canvas);
   controls.target.set(0, 0, 0);
   controls.update();

   const objects = [];

   const scene = new THREE.Scene();
   scene.background = new THREE.Color(0xAAAAAA);

   // Set directional lights
   {
      const color = 0xFFFFFF;
      const intensity = 0.5;
      const decay = 0.5;
      const light = new THREE.SpotLight(color, intensity, decay);
      light.position.set(-10, 10, 0);
      scene.add(light);
   }

   {
      const color = 0xFFFFFF;
      const intensity = 1;
      
      const light = new THREE.DirectionalLight(color, intensity);
      light.position.set(10, 5, 0);
      scene.add(light);
   }

   function addObject(x, y, z, obj) {
      obj.position.x = x ;
      obj.position.y = y ;
      obj.position.z = z ;
  
      scene.add(obj);
      objects.push(obj);
   }

   function addSolidGeometry(x, y, z, geometry, mat) {
      const mesh = new THREE.Mesh(geometry, mat);
      addObject(x, y, z, mesh);
   }

   /** Geometries **/
   {
      const radius = 2;
      const widthSegments = 12;
      const heightSegments = 12;
      //const sphgeom = new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments);
      const sphmat = new THREE.MeshBasicMaterial( {color: 0xffff00} );
      addSolidGeometry(-6, -3, -12, new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments), sphmat);
   }

   {
      const radius = 2;
      const widthSegments = 12;
      const heightSegments = 12;
      //const sphgeom = new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments);
      const sphmat = new THREE.MeshLambertMaterial( {color: 0xff0000} );
      addSolidGeometry(0, -3, -6, new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments), sphmat);
   }

   {
      const radius = 2;
      const widthSegments = 12;
      const heightSegments = 12;
      //const sphgeom = new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments);
      const sphmat = new THREE.MeshPhongMaterial( {
         color: 0x00ff00,
         shininess: 100
      } );
      addSolidGeometry(6, -3, 0, new THREE.SphereBufferGeometry(radius, widthSegments, heightSegments), sphmat);
   }

   {
      const loader = new THREE.FontLoader();
      loader.load('https://threejsfundamentals.org/threejs/resources/threejs/fonts/helvetiker_regular.typeface.json', (font) => {
         const geometry = new THREE.TextBufferGeometry('materials', {
            font: font,
            size: 1,
            height: .2,
            curveSegments: 12,
            bevelEnabled: false,
            bevelThickness: 0.15,
            bevelSize: .3,
            bevelSegments: 5,
         });
         const tmat = new THREE.MeshStandardMaterial( {
            color: 0x33fff9 ,
            metalness: 0.5
         } );
        const mesh = new THREE.Mesh(geometry, tmat);
        geometry.computeBoundingBox();
        geometry.boundingBox.getCenter(mesh.position).multiplyScalar(-1);
  
        const parent = new THREE.Object3D();
        parent.add(mesh);
  
        addObject(-1, 2, 0, parent);
      });
   }

   function resizeRendererToDisplaySize(renderer) {
      const canvas = renderer.domElement;
      const width  = canvas.clientWidth;
      const height = canvas.clientHeight;
      const needResize = canvas.width !== width || canvas.height !== height;
      if (needResize) {
         renderer.setSize(width, height, false);
      }
      return needResize;
   }

   function render(time) {
      time *= 0.001;  // convert time to seconds

      if (resizeRendererToDisplaySize(renderer)) {
         const canvas = renderer.domElement;
         camera.aspect = canvas.clientWidth / canvas.clientHeight;
         camera.updateProjectionMatrix();
      }

      objects.forEach((obj, ndx) => {
         const speed = 1 + ndx * .001;
         const rot = time * speed;
         obj.rotation.x = rot;
         obj.rotation.y = rot;
         obj.rotation.z = rot;
      });

      //renderer.render(bgScene, camera);
      //bgMesh.position.copy(camera.position);

      renderer.render(scene, camera);

      requestAnimationFrame(render);
   }
   requestAnimationFrame(render);
}
main();